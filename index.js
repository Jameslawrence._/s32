const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 4000;

//database information and connection. 
const dbName = "course-booking"
const dbUser = "admin"
const dbPass = "admin131"
const url = `mongodb+srv://${dbUser}:${dbPass}@personaldatabase.gqla2.mongodb.net/${dbName}?retryWrites=true&w=majority`;
const connectStatus = mongoose.connection;


mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true})

connectStatus.on('error', console.error.bind(console, "Error: Failed to connect to the database"))
connectStatus.once('open', () => {
	console.log("Connected to Mongo DB Atlas")
})


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());



app.listen(port, () => {
	console.log(`Server is running at port ${port}`);
})